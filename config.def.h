/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;    /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const int focusonwheel       = 0;
static const char *fonts[]          = { "monospace:size=9" };
static const char col_gray1[]       = "#000000";
static const char col_gray2[]       = "#000000";
static const char col_gray3[]       = "#ffffff";
static const char col_gray4[]       = "#ffffff";
static const char col_cyan[]        = "#00770d";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class        instance    title       tags mask     isfloating   monitor */
	{ "launcher",   NULL,       NULL,       0,            1,           -1 },
	{ "taskmaster", NULL,       NULL,       0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define CMD(...) { .v = (const char *[]){ __VA_ARGS__, NULL } }

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          SHCMD("j4-dmenu-desktop --display-binary --no-generic --usage-log ~/.j4-dmenu-desktop.log") },
	{ MODKEY,                       XK_p,      spawn,          SHCMD("sel=$(cd ~/pass; find * -type f | sed \"s/\\.gpg//\" | dmenu); pass show -c \"$sel\"; sleep 1; setxkbmap; pass show \"$sel\" | grep -oP 'Username: \\K.*' | tr -d '\\n' | xdotool type --clearmodifiers --file -") },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          SHCMD("cd ~/pass; find * -type f | sed \"s/\\.gpg//\" | dmenu | xargs pass otp -c") },
	{ MODKEY,                       XK_Return, spawn,          CMD("st") },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          CMD("thunar") },
	{ MODKEY,                       XK_o,      spawn,          SHCMD("case $(echo \"poweroff\nreboot\nlock\" | dmenu) in 'poweroff') systemctl poweroff;; 'reboot') systemctl reboot;; 'lock') slock;; esac") },
	{ MODKEY,                       XK_i,      spawn,          SHCMD("st -g 73x15 -c \"taskmaster\" -e sh -c \"taskmaster /home/michaellu/.local/share/taskmaster/state.csv\"") },
	{ MODKEY,                       XK_u,      spawn,          SHCMD("cd ~/.screenlayout; find * -type f | dmenu | xargs sh") },
	{ MODKEY,                       XK_e,      spawn,          CMD("dunstctl", "close") },
	{ MODKEY|ShiftMask,             XK_e,      spawn,          CMD("dunstctl", "action", "0") },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_period, incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_comma,  incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      switchcol,      {0} },
	{ MODKEY,                       XK_l,      switchcol,      {0} },
	{ MODKEY|ShiftMask,             XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY|ShiftMask,             XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_space,  zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_f,      togglefloating, {0} },
	{ MODKEY,                       XK_BackSpace,  focusmon,   {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_BackSpace,  tagmon,     {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_x,      quit,           {0} },

	{ 0,                       XF86XK_MonBrightnessUp,      spawn,          CMD("brightnessctl", "s", "+10%") },
	{ 0,                       XF86XK_MonBrightnessDown,    spawn,          CMD("brightnessctl", "s", "10%-") },
	{ 0,                       XF86XK_AudioLowerVolume,     spawn,          CMD("wpctl", "set-volume", "@DEFAULT_AUDIO_SINK@", "5%-") },
	{ 0,                       XF86XK_AudioRaiseVolume,     spawn,          CMD("wpctl", "set-volume", "@DEFAULT_AUDIO_SINK@", "5%+") },
	{ 0,                       XF86XK_AudioMute,            spawn,          CMD("wpctl", "set-mute", "@DEFAULT_AUDIO_SINK@", "toggle") },
	{ 0,                       XK_Print,                    spawn,          SHCMD("maim -s | tee /tmp/area-$(date +%F_%T).png | xclip -selection clipboard -t image/png") },
	{ MODKEY,                  XK_Print,                    spawn,          SHCMD("maim -i $(xdotool getactivewindow) /tmp/window-$(date +%F_%T).png") }
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

